# Projet Rennes

Projet de suivi en tant réel du trafic routier dans Rennes Métropole.
-   Groupe : Riad, Rova, Serge
-   Maintainer, dev : Riad, Rova, Serge (Owner)
## Exemple de rendu obtenu
![Carte de l'état du trafic routier rennais](Etat_du_trafic_en_temps_réel_Rennes_Metropole.png)
## Schéma d'architecture fonctionnelle
![Schéma d'architecture fonctionnelle](./model.svg)
### Pré-requis
#### Python
``` 
cd projet-rennes ;
mkdir venv;
python -m venv venv/; # création envirennement virtuel python
# Depuis un terminal sur Windows (ex. powershell)
& "venv/Scripts/Activate.ps1"
# Depuis vscode (si installé)
code . ; 
# Installation des paquets requis Python (Dans le terminal)
python -m pip install -r requirements.txt

```
#### Docker
-   Installer docker
-   Installer les images ElasticStack (7.9.1)
    -   ```docker pull docker.elastic.co/elasticsearch/elasticsearch:7.9.1```
    -   ```docker pull docker.elastic.co/kibana/kibana:7.9.1```

##### Démarrage

Lancer le stack avec docker stack deploy:

1.  Avec docker stack deploy 
    
    - ```docker stack deploy --prune -c docker-compose.yml etat-trafic-routier-rennais ```
2.  Avec docker compose : 
    
    - ``` docker compose -f docker-compose.yml --detach --force-recreate --build -V --remove-orphans up```
##### Arrêt

- Si lancé avec docker stack deploy :   ```docker stack remove etat-trafic-routier-rennais ```
- Si lancé avec docker compose :  ``` docker compose down```


#### Gestion du processus d'alimentation

1.  Démarrage
    - ``` python process.py --start ```

2.  Arrêt
    -  ``` python process.py --stop ```

3.  Redémarrage
    - ``` python process.py --restart ```

4.  Aide
    - ``` python process.py -h, --help ```
