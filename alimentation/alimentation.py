
import requests, json
from pathlib import Path
from elasticsearch import Elasticsearch

def prepare_data(data, index_name):
    """ Prepare data for bulk api request """
    text=''
    nb_records = len(data['records'])
    if nb_records < 1 :
        text = None
        return text

    for i in range(nb_records) :
        text += json.dumps({ "index" : { "_index" : index_name , "_id" : i+1 } })+'\n' + json.dumps(data["records"][i])+'\n'

    return text


def feed_els_bulk(url_data, param, url_elk, index_name, mapping=None):
    """ Feed Elasticsearch, hosted at url_elk,  with data from url_data at index index_name """
    data = requests.get(url_data, params=param).json()
    prepared_data = prepare_data(data, index_name)

    if data["records"] != [] :
        es = Elasticsearch([url_elk])
        es.bulk(body=prepared_data, index=index_name, headers={"Content-Type":"application/x-ndjson"})
        return True
    else:
        print("Empty Response from API")
        return False

if __name__ == "__main__":
    pass
