- **Description**: generation of the docker image of the backend

- **Build**: `docker build -t feeder:poc-rrs .`

- **Run**: `docker run -it --rm feeder --start`
- **Restart**: `docker run -it --rm feeder --restart`
- **Stop**: `docker run -it --rm feeder --stop`
