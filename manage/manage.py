import os, subprocess, signal, platform
from pathlib import Path

pltform = platform.system()
test=True
if pltform == 'Linux':
    test=False

## Where to store process id
BASE_DIR = Path(__file__).resolve().parent.parent
filename =  os.path.join(BASE_DIR, '.pid')

def stop():
    """ Stop process if running and delete filename containing its pid """
    if os.path.exists(filename):
        with open(filename, "r") as myfile:
            try:
                os.kill(int(myfile.read()), signal.SIGTERM)
            except:
                pass
        
        os.remove( filename )

def start():
    """ Stop process if running, delete filename containing its pid then create start a new one """
    stop()
    command =  subprocess.Popen(["python","-W", "ignore", os.path.join(BASE_DIR,"poc.py")], shell=test)

    
if __name__ == "__main__":
    pass
