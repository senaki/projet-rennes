from datetime import datetime, timedelta
import sched, time, os
from alimentation import alimentation as feed
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent
PID_FILENAME =  os.path.join(BASE_DIR, '.pid')

# Index to create and update
index_name="etat-trafic-routier-rennais"

# delay fetching by 1 min with regard to Rennes API refresh frequency
interval=360 

# Elasticsearch host 
els_url = r"http://localhost:9200"

# Rennes Metropole API
url_rennesmetro = r"https://data.rennesmetropole.fr"
search_api_endpoint = r"/api/records/1.0/search"

# POC Queries
request_parameters={
    "dataset": r"etat-du-trafic-en-temps-reel",
    "q": r"traveltimereliability>=50",
    "rows": 1000,
}

url = url_rennesmetro + search_api_endpoint 

## TO-DO : Prepare data for Elasticsearch with a mapping
# Serge
mapping={
    "mappings": 
    {
        "properties":
        {
            "fields":
            {
                "properties":
                    {
                        "datetime":{"type":"date"},
                        "geo_point_2d":{"type":"geo_point"}, 
                        "geo_shape":{"type":"geo_shape"}
                    }
            }, 
                "geometry":
                {
                    "properties":
                    {
                        "coordinates":{"type":"geo_point"}
                    }
                }
        }
    }
}

query_road = (url, request_parameters, els_url, index_name, mapping)
#query_road = (url, els_url, index_name, mapping)
if feed.Elasticsearch([els_url]).indices.exists(index=index_name) == True :
    feed.Elasticsearch([els_url]).indices.delete(index=index_name)
    feed.Elasticsearch([els_url]).indices.create(index=index_name, body=mapping)

with open(PID_FILENAME, "w") as myfile:
    myfile.write( str(os.getpid()) )
# Entering main loop
FLAG=True
event_schedule = sched.scheduler(time.time, time.sleep)
while True:
    if(FLAG):
        delay = 1
        FLAG=False
        
    else:
        delay = interval

    event_schedule.enter(delay, 1, feed.feed_els_bulk, query_road )
    event_schedule.run()
