#!/usr/bin/env python
import argparse
from manage import manage   

## Parser
parser = argparse.ArgumentParser(
    description="Start/stop poc.py program, a proof of concept of traffic state monitoring.",
    usage="""
- process.py --start
- process.py --stop
""")

parser.add_argument("--start", help="start poc.py", action="store_true")
parser.add_argument("--stop", help="stop poc.py", action="store_true")
parser.add_argument("--restart", help="restart poc.py", action="store_true")

args = parser.parse_args()

if args.start:
    manage.start()

if args.stop:
    manage.stop()

if args.restart:
    manage.stop()
    manage.start()

